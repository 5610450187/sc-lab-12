package student;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;


public class HWScore {
	double[] listscore;
	public HWScore() {
		String filename = "homework.txt";
		FileReader fileReader = null;
		BufferedReader buffer;
		FileWriter fileWriter = null;
		PrintWriter out;
		listscore = new double[5];

		try{
			// read from user
			fileReader = new FileReader(filename);
			buffer = new BufferedReader(fileReader);
			//write to file
			fileWriter = new FileWriter("average.txt");
			out = new PrintWriter(fileWriter);
			out.println("--------- Homework Score ---------"+"\n"+"Name Average"+"\n"+"==== =====");
			String line;
			for(line = buffer.readLine(); line != null; line = buffer.readLine()){
				String[] data = line.split(",");
				String name = data[0].trim();
				listscore[0] = Double.parseDouble(data[1].trim());
				listscore[1] = Double.parseDouble(data[2].trim());
				listscore[2] = Double.parseDouble(data[3].trim());
				listscore[3] = Double.parseDouble(data[4].trim());
				listscore[4] = Double.parseDouble(data[5].trim());
				Student std = new Student(name, listscore);
				std.averageHW(listscore);
				out.println(std.toString());
			}
			out.flush();
		}catch(FileNotFoundException e){
			System.err.println("Cannot read file "+filename);
		}catch(IOException e){
			System.err.println("Error reading from file");
		}finally {
			try {
				if (fileReader != null)
					fileReader.close();
			} catch (IOException e) {
				System.err.println("Error closing files");
			}
		}

	}

}
