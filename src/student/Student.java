package student;

public class Student {
	private String name;
	private double[] listscore;
	private double sum=0;
	private double total = 0;
	public Student(String name , double[] listscore) {
		this.name = name;
		this.listscore = listscore;
	}
	
	public double averageHW(double[] listscore){
		for (double d : listscore) {
			sum+=d;
		}
		total =sum/5;
		return total;
	}
	public double averageExam(double[] listscore){
		for (double d : listscore) {
			sum+=d;
		}
		total =sum/2;
		return total;
	}
	public String toString(){
		return name+" "+total;
	}

}
