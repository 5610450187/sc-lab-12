package phonebook;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class PhoneBook {
	ArrayList<PhoneBookEntry> list;
	public PhoneBook() {
		String filename = "phonebook.txt";
		FileReader fileReader = null;
		BufferedReader buffer;
		list = new ArrayList<PhoneBookEntry>();
		try{
			fileReader = new FileReader(filename);
			buffer = new BufferedReader(fileReader);
			System.out.println("--------- Java Phone Book ---------"+"\n"+"Name Phone"+"\n"+"==== =====");
			String line;
			for(line = buffer.readLine(); line != null; line = buffer.readLine()){
				String[] data = line.split(",");
				String name = data[0].trim();
				String number = data[1].trim();
				
				PhoneBookEntry pb = new PhoneBookEntry(name, number);
				addEntryName(pb);
				list.add(pb);
				System.out.println(pb.toString());
			}
		}catch(FileNotFoundException e){
			System.err.println("Cannot read file "+filename);
		}catch(IOException e){
			System.err.println("Error reading from file");
		}finally {
			try {
				if (fileReader != null)
					fileReader.close();
			} catch (IOException e) {
				System.err.println("Error closing files");
			}
		}

	}
	public void addEntryName(PhoneBookEntry p){
		list.add(p);
	}
	public void removeEntryName(String name){
		for (PhoneBookEntry phoneBookEntry : list) {
			if(phoneBookEntry.equals(name)){
				list.remove(name);
			}
		}
	}
	public void search(String name){
		for (PhoneBookEntry phoneBookEntry : list) {
			if(phoneBookEntry.equals(name)){
				phoneBookEntry.toString();
				
			}
		}
	}
}
